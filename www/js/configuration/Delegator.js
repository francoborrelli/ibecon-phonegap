class Delegator {
  startRanging() {
    throw new Error("This method should be implemented");
  }

  stopRanging() {
    throw new Error("This method should be implemented");
  }

  startMonitoring() {
    throw new Error("This method should be implemented");
  }

  stopMonitoring() {
    throw new Error("This method should be implemented");
  }
}
